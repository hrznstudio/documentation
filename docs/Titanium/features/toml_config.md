## Annotation based TOML config

To start using the system you need to register the class where all the config values are in the mod constructor with `addConfig(AnnotationConfigManager.Type type)`. The class specified in the Type needs to have all the config values in `public static` to work properly. All the variables that want to be secified as a config value need to have the annotation `@ConfigVal` present for the system to work. The variable name will be used as a key in the config file and the value of the variable as a default value.

That annotation can have to optional properties:

* `String comment`: That will use that string as a comment in the config file
* `String value`: If present it will override the variable name as a key in the config file

Numeric variables can have an extra annotation to define their valid range `(InRangeInt, InRangeLong, InRangeDouble)`

*Nested classes can be used*