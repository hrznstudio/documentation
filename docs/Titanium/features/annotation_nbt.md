## TileEntity field saving with Annotations

TileEntities fields can be saved and synced by having the `@Save` annotation on the field. That will automatically save the field and sync to the client. You can check all the NBT handlers added in the `NBTManager` class, if a field is missing a manager titanium will crash intentionally as a warning.



*This will only work if the block containing the tile has been added with `addBlock` in the mod constructor or has been added manually with `NBTManager.getInstance().scanTileClassForAnnotations(Class<? extends TileEntity> entity)`*