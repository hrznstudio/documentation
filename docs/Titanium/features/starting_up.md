## Starting Up With Titanium

Your main mod class should extend `TitaniumMod`, a class that will help you to setup things easily. In the class constructor the following things should be done:


#### Registering Registry Entries
Using `addEntry(Class<IForgeRegistryEntry>, IForgeRegistryEntry entry)` will allow you to register you registry entries automatically, including their models, tiles and colors.

##### Example
```java
addEntry(Block.class, new BlockTest());
addEntry(Item.class, new ItemTest());
```

#### Registering your config files
Titanum allows you to have Annotation based config classes but for that you need to register the class for that using `addConfig(AnnotationConfigManager.Type type)` specifying the type of config and class. More information here.
